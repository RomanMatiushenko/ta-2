//1
const exectly_Age = () => {
    const birth_Date = new Date();
    return new Date(Date.now() - birth_Date).getUTCFullYear() - 1970;
};

//2
const get_Week_Day = () => {
    let date = new Date();
    let weekdays = [
        'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
    ];
    let weekDay = date.getDay();

    return weekdays[weekDay];
};

//3
const get_Amount_Days_ToNewYear = () => {
    let today = new Date();
    let newY = new Date(today.getFullYear(), 11, 31);
    if (today.getMonth() === 11 && today.getDate() > 31) {
        newY.setFullYear(newY.getFullYear() + 1);
    }
    let one_day = 1000 * 60 * 60 * 24;
    return Math.ceil((newY.getTime() - today.getTime()) / one_day);
};

//4
const get_Prog_Day = () => {
    let date = new Date();
    let Prog_Day = new Date(date.getFullYear(), 8, 13);
    let weekdays = [
        'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
    ];
    let weekDay = date.getDay();
    return Prog_Day.getFullYear() + Prog_Day.getMonth() + Prog_Day.getDay() + weekdays[weekDay];
};

//5
const how_Far_Is = (day) => {
    let date = new Date();
    let weekdays = [
        'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
    ];
    let weekDay = date.getDay();
    let farday = weekdays[weekDay];
    let number = weekdays.length - weekDay;
    return farday === day ? `Hey, today is ${ farday } =)` : `It's ${ number } day(s) left till ${ farday }`

};


//6
const is_Valid_Identifier = str => {
    const myReg = /^[a-z_$][a-z0-9_$]*$/i;
    return myReg.test(str);
};

//7
const test_String = teststr => {
    return teststr.replace(/^(.)|\s+(.)/g, letter => letter.toUpperCase());
};

//8
const is_Valid_AudioFile = format => {
    const myReg = /^[a-z0-9_$]*$/i;
    return myReg.test(format);
}

//9
const get_Hexadecimal_Colors = teststr => {
    return teststr;
};

//10
const is_Valid_Password = teststr => {
    return teststr;
};

//11
const add_Thousands_Separators = () => {
    return;
};

//12
const get_All_Urls_FromText = () => {
    return;
};