//1
const isEqual = (el1, el2) => Object.is(el1, el2);



//2
const isBigger = (el1, el2) => el1 > el2;


//3
const storeNames = (...names) => {
    const ARR = [];
    ARR.push(...names);
    return ARR;
}

//4
const getDifference = (el1, el2) => el1 > el2 ? el1 - el2 : el2 - el1;

//5
const negativeCount = (ARR) => {
    const NEW_ARR = [];
    for (const el of ARR) {
        NEW_ARR.push(el < 0)
    }
    return NEW_ARR.length;
}


//6

const lettercount = (word, letter) => word.toLowerCase().split('').filter(char => char === letter).length;

//7
const countPoints = () => {
    let newArr;
    return newArr
}