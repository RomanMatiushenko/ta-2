const MIN_NUMBER = 0;
const INIT_MAX_NUMBER = 4;
const ADDITIONAL_MAX_PER_ROUND = 4;
const ATTEMPT_COUNT = 3;

const PRIZE = 100;

let totalPrize = 0;
let count = 0;

const start = (round) => {
    const lastWord = count === 0 ? 'a game' : 'again';
    count += 1;

    if (round === 1) {
        const isPlay = confirm(`Do you want to play ${lastWord}?`);

        if (!isPlay) {
            alert('You did not become a billionaire, but can.');
            return;
        }
    }

    // round += 1;
    const currentPrize = round === 1 ? PRIZE : 2 * PRIZE;

    const max = INIT_MAX_NUMBER + round * ADDITIONAL_MAX_PER_ROUND;
    const value = Math.floor(Math.random() * max) + 1;
    console.log(MIN_NUMBER, max, value);

    let attempt = 1;
    while (attempt <= ATTEMPT_COUNT) {
        const guess = prompt(`Choose a roulette pocket number from ${MIN_NUMBER}-${max}\r
    Attempt#: ${attempt}\r
    Total prize: ${totalPrize}\r
    Possible prize on current attempt: ${currentPrize}$`);

        if (parseInt(guess, 10) === value) {
            const prize = currentPrize / Math.pow(2, attempt - 1);
            totalPrize += prize;

            const oneMore = confirm(`Congratulation, you won!\r
             Your prize is: ${prize}$.\r
         Do you want to continue?`);

            if (oneMore) {
                start(round + 1);
            }
            break;
        }
        attempt += 1;
    }
    alert(`Thank you for your participation.\r
    Your prize is: ${totalPrize}`);
    totalPrize = 0;
    start(1);
};

start(1);