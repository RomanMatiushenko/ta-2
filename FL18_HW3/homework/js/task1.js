const Persentage = 100;
const MinAmount = 1000;
let initialAmount;
let guess1 = prompt('Please, enter Initial Amount (more than 1000): ');
if (isNaN(guess1)) {} else if (guess1 < MinAmount) {} else {
    initialAmount = parseFloat(guess1).toFixed(2);
}
let numberOfYears;
let guess2 = prompt('Please, enter Number of Years (more than 0): ');
if (isNaN(guess2)) {} else if (guess2 < 1) {} else {
    numberOfYears = parseInt(guess2);
}
let percentageOfYear;
let guess3 = prompt('Please, enter Percentage of Year (less than 100): ');
if (isNaN(guess3)) {} else if (guess3 > Persentage) {} else {
    percentageOfYear = parseFloat(guess3).toFixed(2);
}
let messageAlert = 'Initial Amount: ' + initialAmount + "\r\n" +
    'Number of Years: ' + numberOfYears + "\r\n" +
    'Persentage of Year: ' + percentageOfYear;
if (numberOfYears === undefined || initialAmount === undefined || percentageOfYear === undefined) {
    alert('Invalid input data');
} else {
    let forYearsTotalAmount = (initialAmount * (1 + percentageOfYear / Persentage) ** numberOfYears).toFixed(2);
    let forYearsTotalProfit = (forYearsTotalAmount - initialAmount).toFixed(2);
    alert(messageAlert + "\r\n" +
        'Total profit: ' + forYearsTotalProfit + "\r\n" + 'Total amount: ' + forYearsTotalAmount);
}