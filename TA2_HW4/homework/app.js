function reverseNumber(num) {
    return (
        parseInt(
            num
            .toString()
            .split('')
            .reverse()
            .join('')
        ) * Math.sign(num)
    )
}


function forEach(arr, func) {
    for (const el of arr) {
        func(el);
    }
}

function map(arr, func) {
    const newArr = []
    forEach(arr, el => newArr.push(func(el)))
    return newArr;
}


function filter(arr, func) {
    const ArrFilter = []
    forEach(arr, el => func(el) ? ArrFilter.push(el) : false)
    return ArrFilter;
}

function getAdultAppleLovers(data) {
    let adultAge = 18;
    let fruit = 'apple';
    return map(filter(data, (el) => {
            return el.age > adultAge && el.favoritefruit === fruit
        }),
        (el) => el.name)
}


function getKeys(obj) {
    const Keys = [];
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            Keys.push(key)
        }
    }
}

function getValues(obj) {
    const Arr = [];
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            Arr.push(obj[key])
        }
    }
    return Arr
}

function showFormattedDate(dateObj) {
    const day = dateObj.getDate();
    const month = dateObj.toLocaleString('default', { month: 'short' });
    const year = dateObj.getFullYear();
    return `It is ${day}of ${month}, ${year}`
}