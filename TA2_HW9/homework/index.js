/* START TASK 1: Your code goes here */
document.getElementById('task1').addEventListener('click', color);
document.getElementById('trId').addEventListener('click', colorTR);
document.getElementById('specialCell').addEventListener('click', colorSC);

function color() {
    document.getElementById('task1').style.backgroundColor = 'green';
    const table = document.querySelector('#task1');
    table.classList.toggle('green');
}

function colorTR() {
    document.getElementById('trId').style.backgroundColor = 'blue';
    const tr = document.querySelector('#trId');
    tr.classList.toggle('red');
}

function colorSC() {
    document.getElementById('specialCell').style.backgroundColor = 'yellow';
    const sCell = document.querySelector('#specialCell');
    sCell.classList.toggle('yellow');
}

/* END TASK 1 */

/* START TASK 2: Your code goes here */
document.getElementById('task2').addEventListener('click', input);

function input() {
    const number = document.getElementById('number');
    number.addEventListener('input', function() {
        if (number.validity.typeMismatch) {
            number.setCustomValidity('expecting a number!');
        } else {
            number.setCustomValidity('');
        }

    });
}

/* END TASK 2 */

/* START TASK 3: Your code goes here */

/* END TASK 3 */