function visitLink(path) {
    let currentValue = localStorage.getItem(path) ? parseInt(localStorage.getItem(path)) : 0;
    let newValue = currentValue + 1;
    localStorage.setItem(path, newValue);
}

function viewResults() {
    let dataProvider = window.localStorage;

    let page_1 = dataProvider.Page1 ? parseInt(dataProvider.Page1) : 0;
    let page_2 = dataProvider.Page2 ? parseInt(dataProvider.Page2) : 0;
    let page_3 = dataProvider.Page3 ? parseInt(dataProvider.Page3) : 0;
    let result = document.getElementById('content');

    let oldUl = document.getElementById('results-list');

    if (oldUl) {
        oldUl.remove();
    }

    let ul = document.createElement('ul');
    ul.setAttribute('id', 'results-list');

    let page_1_li = document.createElement('li');
    page_1_li.innerHTML = ['You visited Page1 - ', page_1, ' times'];

    let page_2_li = document.createElement('li');
    page_2_li.innerHTML = ['You visited Page2 - ', page_2, ' times'];

    let page_3_li = document.createElement('li');
    page_3_li.innerHTML = ['You visited Page3 - ', page_3, ' times'];

    ul.appendChild(page_1_li);
    ul.appendChild(page_2_li);
    ul.appendChild(page_3_li);

    result.appendChild(ul);

    window.localStorage.clear();
}