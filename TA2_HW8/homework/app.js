const appRoot = document.getElementById('app-root');

const SEARCH_TYPES = {
    region: 'region',
    language: 'language',
};

const onRadioBtnClick = (type) => {
    console.log('onRadioBtnClick', type);

    const select = document.getElementById('selectSearch');
    select.innerHTML = '';

    if (select.disabled) {
        select.removeAttribute('disabled');
        document.getElementById('noItemP').classList.add('none');
    }

    const method = type === SEARCH_TYPES.language ? 'getLanguagesList' : 'getRegionsList';
    const data = externalService[method]();

    data.forEach(item => {
        const option = document.createElement('option');
        option.value = item;
        option.innerHTML = item;
        select.appendChild(option);
    });
}

appRoot.insertAdjacentHTML(
    'afterbegin',
    ` <header>
    <form class="application">
        <h1 class="aplication_title"> Countries Search</h1>
        <div class = "application type-block"></div>

        <p> Please choose the type of search: </p>
        <div class="radio-block">
            <input type="radio" id="regBtn" name="searchType" value="region" class="radioBtn" 
            onclick="onRadioBtnClick(SEARCH_TYPES.region)">
            <label for="regBtn">By Region</label>
            <br>
            <input type="radio" id="lanBtn" name="searchType" value="language" class="radioBtn" 
            onclick="onRadioBtnClick(SEARCH_TYPES.language)">
            <label for="lanBtn">By Language</label>
        </div>

        <div class="aplication_search-block">
            <br>
            <label for="selectSearch">Please choose search query: </label>
            <select id="selectSearch" name="selectSearch" class="width" disabled="disabled">
                <option value="select value" class="width" selected="selected">Select value</option>
            </select>
        </div>

        <p id="noItemP">No items, please choose search query!!!</p>

    </form>
</header >
`);





/*
list of all regions
externalService.getRegionsList();
list of all languages
externalService.getLanguagesList();
get countries list by language
externalService.getCountryListByLanguage()
get countries list by region
externalService.getCountryListByRegion()
*/